<?php
// Verificar si se ha hecho clic en el botón de cerrar sesión
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['cerrar_sesion'])) {
    // Destruir la sesión y redirigir a la página de inicio de sesión
    session_unset();
    session_destroy();
    header("Location: login.php");
    exit();
}
?>