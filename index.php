<!DOCTYPE html>
<html lang="es">
<head>
  <title>TO-DO-LIST</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>
<body>

<div class="container my-5">

  <h2>To Do List</h2>
  
  <div class="row">
    <div class="col-sm-6">
        <?php 
         include("form-to-do.php");
         include("to-do-list.php");
        ?>
    </div>
     <div class="col-sm-6">
     </div>
     <div> 
        <form action="cerrarSesion.php" method="POST">
          <button type="submit" class="btn btn-primary" name="cerrar_sesion">Cerrar Sesión</button>
        </form>
     </div>
  </div>
</div>
</body>
</html>