<!DOCTYPE html>
<html lang="es">
<head>
    <title> Registro</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
</head>
<body>
  
  <div class="m-5 vh-100 container justify-content-center align-items-center">
    <h2>Registro de usuario</h2> 
      <form action="registroUsuario.php" method="POST">
        <div class="mb-3 justify-content-center align-items-center"">
            <label for = "email1" class="form-label"> Correo </label>
            <input type="text" class="form-control" id= "email1" name="correo">
        </div>
        <div class="mb-3 justify-content-center align-items-center"">
          <label for = "contra1" class="form-label"> Contrasenia </label>
          <input type="text" class="form-control" id= "contra1" name="contra">
        </div>
        
        <button type="submit" class="btn btn-primary">Registrarse</button>
      </form>
  </div>
</body>
</html>