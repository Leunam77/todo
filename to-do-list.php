<?php

include('obtener-to-do.php');
error_reporting(E_ALL & ~E_NOTICE);
error_reporting(E_ALL & ~E_WARNING);
$getTask = getTask(); 
?>

<?php
if(count($getTask['data'])) {
foreach($getTask['data'] as $task) {
?>

<div class="row my-3">
    <div class="col-sm-10">
        <?php
       echo $task['task'];
        ?>
    </div>
    <div class="col-sm-1">
                <input type="checkbox" name="task_<?php echo $task['id']; ?>" id="task_<?php echo $task['id']; ?>">
    </div>
    
    <div class="col-sm-1">
        <a href="index.php?edit-task=<?php echo $task['id']; ?>" class="text-success text-decoration-none">
        <span class="fas fa-edit"></span>
        </a>
    </div>

    <div class="col-sm-1">
    <a href="delete.php?delete=<?php echo $task['id']; ?>" class="text-danger text-decoration-none">
    <span class="fas fa-trash-alt" ></span>


    </a>
    </div>
</div>
<hr>

<?php 
} 
}
?>